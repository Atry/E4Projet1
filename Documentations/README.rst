Le projet est Alex-Studio

Version 0.8.0


Le but de ce projet est de crée une playlist aléatoire générée dans un fichier M3U qui permettra d'aller très vite pour crée des playlist, on demande le genre que l'on veut avec le nombre de pourcentage que l'on veut de ce genre dans la playlist.

Ces instructions vous permettront d'obtenir une copie opérationnel sur votre machine à des fins d'utilisation personnel, de test.
Cela permet de déployer le projet sur un système actif.
Conditions préalables.

De quoi avez-vous besoin pour installer le logiciel et comment l'installer ?

Pour installer le projet, il vous suffit de:
        - Décompresser le fichier zip telecharger
        - Ouvrir le terminal
        - Se déplacer dans le dossier de l'instalation, la ou ce trouve tout les fichier du projet.
           ecrire dans le terminal make && ./generateur_playlist + option --h pour voir les option

Exemple recherche d'un genre et de sa quantité :
make && ./generateur_playlist -g Rock -G 10

Apres avoir fait l'action ci dessus
Exemple d'extraction d'une donnée:
        renvoie le "titre": for the damaged ; "l'album" Melody Of Certain Damaged Lemons ; "l'Artiste" Blonde Redhead ; "le genre" Rock ; la duree 157 ; le format audio Fichier audio MPEG ; la polyphonie 2 ;
        et "le chemin" /home/radio/musique/Blonde Redhead/Melody Of Certain Damaged Lemons/11 for the damaged.mp3 ;



Les contributeurs:
        ATRY Alexandre
        MARQUES Alexandre

Ce projet est sous licence GNU GPL - voir le dossier Licence pour trouver les licences FR et EN.



