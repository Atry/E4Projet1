#ifndef TOOLS_H
#define TOOLS_H

#include <cctype>
#include <limits>

bool saisie_oui_non(const char *question, char yes = 'o', char no = 'n')
{
  char input = '\0';
  bool output = false;
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

  while(std::tolower(input) != yes
        && std::tolower(input) != no)
  {
    std::cout << question << " [" << yes << "/" << no << "] : ";
    std::cin.get(input);

    if(input != '\n')
    {
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
  }

  return (input == 'o' || input == 'O');
}

#endif // TOOLS_H
