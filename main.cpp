#include <postgresql/libpq-fe.h>
#include <string>
#include <syslog.h>
#include <fstream>
#include <iostream>
#include <cstdlib>

#include "tools.h"
#include "cli.h"

int main(int nombre_arguments, char **tab_arguments)
{
  PGconn *conn;
  PGresult *res;		// Variable de connexion.
  PGresult *res2;
  PGresult *res3;
  int dureeTotale = 0;
  int pourcentage = 0;
  int dureePourcent = 0;
  int dureeMusic = 0;
  //int dureeTempo = 0;
  int i = 0;
  std::string genre = "";
  bool ouiounon;
  std::string const nomFichier("playlist.m3u"); //Ouvrir un fichier en lecture et ecriture.
  std::ofstream monFlux(nomFichier.c_str()); //La variable monFlux sera le fichier.
  struct arguments struct_des_arguments;
  argp_parse(&argp, nombre_arguments, tab_arguments, 0, 0, &struct_des_arguments);

  if(monFlux)
  {
    openlog("principal", LOG_CONS | LOG_PID, LOG_USER);
    syslog(LOG_DEBUG, "Programme lancé");
    closelog();
    conn = PQconnectdb("dbname=radio_libre host=172.16.99.2 port=5432 user=a.atry"); //Conexion BDD

    if(PQstatus(conn) == CONNECTION_BAD)
    {
      std::cout << "Connection refusé" << std::endl;
    }

    //std::cout << "Connection réussie" << std::endl;
    //std::string req("SELECT * FROM morceaux ORDER BY RANDOM() LIMIT 1"); //Recupere une seul musique aleatoirement
    //res = PQexec(conn, req.c_str());
    //std::cout << PQgetvalue(res, i, 8) << std::endl; //Affiche le chemin de la musique
    std::string req2("SELECT genre FROM morceaux GROUP BY genre"); //Récupere toute la liste des genres
    res2 = PQexec(conn, req2.c_str());
    //std::cout << PQgetvalue(res, i, 8) << std::endl; //Affiche le chemin de la musique
    std::cout << "Bienvenue sur le générateur de playlist de Alex-Studio \n" << std::endl;
    std::cout << "Développer par Alexandre ATRY et Alexandre MARQUES\n\n" << std::endl;
    std::cout << "Saisir une durée pour la playlist en minutes: ";
    std::cin >> dureeTotale;	//Insertion de la durée
    dureeTotale = dureeTotale * 60; //Convertisseur en secondes.
    std::cout << "La duree est de " << dureeTotale << " secondes\n" << std::endl; //Affiche les secondes.
    ouiounon = saisie_oui_non("Voulez vous choisir un genre musical ?"); //Demande si oui ou non on veux un genre.

    if(dureeTotale >= 30)
    {
      while(ouiounon)
      {
        std::cout << "Saisir un genre parmis ces propositions:\n " << std::endl;

        for(i = 0; i < PQntuples(res2); i++)  //Affiche tout les tuples retourner par la requête res2 en partant de 0
        {
          std::cout << PQgetvalue(res2, i, 0) << " _ "; //Affiche les résultats de la requête res2 dans le terminal
        }

        std::cout << std::endl;

        //std::cin >> genre; //Saisir le genre voulu parmis la liste retourner.
        getline(std::cin, genre);
        std::cout << std::endl;
        std::cout << "Saisir un pourcentage pour le " << genre << std::endl;
        std::cin >> pourcentage; // Saisie pourcentage

        if(pourcentage > 100)
        {
          pourcentage = 100;
        }

        std::cout << std::endl;
        dureePourcent = dureeTotale * pourcentage / 100; //Convertisseur pour obtenir le nombre de secondes
        std::cout << "La duree est de " << dureePourcent << " secondes\n" << std::endl;
        std::cout << "Generation de musique du genre choisis" << std::endl << std::endl;
        std::string req("SELECT * FROM morceaux WHERE genre ='%s' ORDER BY RANDOM()"); //Recupere une seul musique aleatoirement
        req.replace(37, 2, genre);
        //std::cout << req << std::endl;
        res = PQexec(conn, req.c_str());

        if(PQresultStatus(res) != PGRES_TUPLES_OK)
        {
          fprintf(stderr, "SELECT query failed.\n");
          PQclear(res);
          PQfinish(conn);
          exit(1);
        }

        unsigned int somme = 0;

        for(int compteur = 0; compteur < PQntuples(res); ++compteur)
        {
          dureeMusic = atoi(PQgetvalue(res, compteur, 5)); // On récupère, en tant qu'entier, la durée du tuple en secondes

          if(dureeMusic + somme <= dureePourcent)
          {
            std::cout << PQgetvalue(res, compteur, 8) << std::endl;
            monFlux << PQgetvalue(res, compteur, 8) << std::endl;
            somme += dureeMusic;
          }
        }

        dureeTotale = dureeTotale - somme;
        std::cout << "Il reste " << dureePourcent - somme << " secondes de pourcentage" << std::endl << std::endl << std::endl;
        std::cout << "Il reste " << dureeTotale << " secondes au total" << std::endl << std::endl << std::endl;
        ouiounon = saisie_oui_non("Voulez vous choisir un genre musical ?"); //Demande si oui ou non on veux un genre.
      }

      std::string req3("SELECT * FROM morceaux ORDER BY RANDOM()"); //Recupere une seul musique aleatoirement
      res3 = PQexec(conn, req3.c_str());
      std::cout << "Generation de la musique aléatoirement" << std::endl << std::endl;

      if(PQresultStatus(res3) != PGRES_TUPLES_OK)
      {
        fprintf(stderr, "SELECT query failed.\n");
        PQclear(res3);
        PQfinish(conn);
        exit(1);
      }

      unsigned int somme2 = 0;

      for(int compteur2 = 0; compteur2 < PQntuples(res3); ++compteur2)
      {
        dureeMusic = atoi(PQgetvalue(res3, compteur2, 5)); // On récupère, en tant qu'entier, la durée du tuple en secondes

        if(dureeMusic + somme2 <= dureeTotale)
        {
          std::cout << PQgetvalue(res3, compteur2, 8) << std::endl;
          monFlux << PQgetvalue(res3, compteur2, 8) << std::endl;
          somme2 += dureeMusic;
        }
      }

      dureeTotale = dureeTotale - somme2;
      std::cout << "Il reste " << dureeTotale << " secondes au total" << std::endl << std::endl << std::endl;
    }
    else
    {
      std::cout << "Génération de la playlist terminer.";
    }

    PQfinish(conn); /* disconnect from the database */
  }
  else
  {
    std::cout << "ERREUR: Impossible d'ouvrir le fichier." << std::endl;
  }

  return 0;
}
