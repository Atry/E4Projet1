#ifndef CLI_H
#define CLI_H

#include <iostream>
#include <string>
#include <cstring>
#include <argp.h>

const char *argp_program_version = "generateur_playlist v0.1";
const char *argp_program_bug_address = "<alexandre.marques@bts-malraux.net>";

/*program documentation*/
static std::string program_description = "Générateur de playlist au format M3U, XSPF, etc.";

/*Vector (= table) which contains the definition of the possible options*/
/*possible option */
static struct argp_option vecteur_des_options[] =
{
  /* "name"  "key" "name of argument" "flags" "documentation" "groups" */
  {"duree", 'd', "temps", 0, "definie la duree du titre ", 0},
  {"genre", 'g', "expression", 0, "Spécifie le critère genre à sélectionner", 0},
  {"qteGenre", 'G', "entier", 0, "Quantité du genre à intégrer", 0},
  {"sous-genre", 's', "sousexpresion", 0, "Spécifie le critère sous-genre à sélectionner", 0},
  {"qteSous-genre", 'S', "entier", 0, "Quantité du sous-genre à intégrer", 0},
  {"artiste", 'c', "auteur", 0, "Spécifie le critère auteur à sélectionner", 0},
  {"qteArtiste", 'C', "auteur", 0, "Quantité de l'artiste à intégrer", 0},
  {"titre", 't', "nom", 0, "Définie le titre à sélectionner", 0},
  {"qteTitre", 'T', "nom", 0, "Quantité du titre à intégrer", 0},
  {"album", 'a', "groupe", 0, "Identifie le critère album à sélectionner", 0},
  {"qteAlbum", 'A', "groupe", 0, "Quantité de l'album à intégrer", 0},
  { 0 }
};

struct arguments
{
  char *args[2];
  char genre[255], sous_genre[255], artiste[255], titre[255], album[255];
  int qte_genre = 100,
      qte_sous_genre = 100,
      qte_artiste = 100,
      qte_titre = 100,
      qte_album = 100;
  int duree = 0;
  char *output_file;
};

/* function that defines the action of the options */
static error_t
actions_des_options(int key, char *arg, struct argp_state *state)
{
  struct arguments *mes_args = (struct arguments *)state->input;

  switch(key)
  {
    case 'd' :
      std::cout << "Option 'duree' choisie" << std::endl;
      if(std::atoi(arg) < 255)
      {
        mes_args->duree = std::atoi(arg);
        std::cout << mes_args->duree << std::endl;
        break;
      }
      else
      {
        return ARGP_ERR_UNKNOWN;
      }

    case 'g' :
      std::cout << "Option 'genre' choisie" << std::endl;

      if(std::strlen(arg) < 255)
      {
        std::strcpy(mes_args->genre, arg);
        std::cout << mes_args->genre << std::endl;
        break;
      }
      else
      {
        return ARGP_ERR_UNKNOWN;
      }

    case 'G' :
      std::cout << "Quantité du 'genre' choisie" << std::endl;

      if(std::strlen(arg) < 4)
      {
        int quantite = std::atoi(arg);

        if(quantite > 0 && quantite <= 100)
        {
          mes_args->qte_sous_genre = quantite;
          std::cout << mes_args->qte_sous_genre << std::endl;
          break;
        }
        else
        {
          return ARGP_ERR_UNKNOWN;
        }
      }

    case 's' :
      std::cout << "Option 'sous-genre' choisie" << std::endl;
      std::cout << mes_args->sous_genre << std::endl;
      break;

      case 'S' :
        std::cout << "Quantité du 'sous-genre' choisie" << std::endl;

        if(std::strlen(arg) < 4)
        {
          int quantite = std::atoi(arg);

          if(quantite > 0 && quantite <= 100)
          {
            mes_args->qte_genre = quantite;
            std::cout << mes_args->qte_genre << std::endl;
            break;
          }
          else
          {
            return ARGP_ERR_UNKNOWN;
          }
        }

    case 'c' :
      std::cout << "Option 'artiste' choisie" << std::endl;
      std::cout << mes_args->artiste << std::endl;

      if(std::strlen(arg) < 255)
      {
        std::strcpy(mes_args->artiste, arg);
        std::cout << mes_args->artiste << std::endl;
        break;
      }
      else
      {
        return ARGP_ERR_UNKNOWN;
      }

  case 'C' :
    std::cout << "Quantité de 'artiste' choisie" << std::endl;

    if(std::strlen(arg) < 4)
    {
      int quantite = std::atoi(arg);

      if(quantite > 0 && quantite <= 100)
      {
        mes_args->qte_artiste = quantite;
        std::cout << mes_args->qte_artiste << std::endl;
        break;
      }
      else
      {
        return ARGP_ERR_UNKNOWN;
      }
    }

    case 't' :
      std::cout << "Option 'titre' choisie" << std::endl;
      std::cout << mes_args->titre << std::endl;

      if(std::strlen(arg) < 255)
      {
        std::strcpy(mes_args->titre, arg);
        std::cout << mes_args->titre << std::endl;
        break;
      }
      else
      {
        return ARGP_ERR_UNKNOWN;
      }

  case 'T' :
    std::cout << "Quantité de 'titre' choisie" << std::endl;

    if(std::strlen(arg) < 4)
    {
      int quantite = std::atoi(arg);

      if(quantite > 0 && quantite <= 100)
      {
        mes_args->qte_titre = quantite;
        std::cout << mes_args->qte_titre << std::endl;
        break;
      }
      else
      {
        return ARGP_ERR_UNKNOWN;
      }
    }

    case 'a' :
      std::cout << "Option 'album' choisie" << std::endl;
      std::cout << mes_args->album << std::endl;

      if(std::strlen(arg) < 255)
      {
        std::strcpy(mes_args->album, arg);
        std::cout << mes_args->album << std::endl;
        break;
      }
      else
      {
        return ARGP_ERR_UNKNOWN;
      }

  case 'A' :
    std::cout << "Quantité de 'album' choisie" << std::endl;

    if(std::strlen(arg) < 4)
    {
      int quantite = std::atoi(arg);

      if(quantite > 0 && quantite <= 100)
      {
        mes_args->qte_album = quantite;
        std::cout << mes_args->qte_album << std::endl;
        break;
      }
      else
      {
        return ARGP_ERR_UNKNOWN;
      }
    }

    default:
      return ARGP_ERR_UNKNOWN;
  }

  return 0;
}

/*argp analyzer */
static struct argp argp =
{
  vecteur_des_options,
  actions_des_options,
  0,
  program_description.c_str(),
  0, 0, 0
};

#endif  // CLI_H
